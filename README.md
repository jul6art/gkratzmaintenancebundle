## Installation

1) //-- add in the composer.json of your project --//

    "require": {
        // ...
        "gkratz/maintenancebundle": "dev-master"
    }


2) //-- activate the bundle in your app/AppKernel.php --//

    new Gkratz\MaintenanceBundle\GkratzMaintenanceBundle(),


3) //-- enter this command in your terminal --//

    php composer update gkratz/maintenancebundle


4) //-- create an empty AppBundle:Maintenance entity with a PROTECTED id and that extends \Gkratz\MaintenanceBundle\Model\Maintenance --//

    <?php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Gkratz\MaintenanceBundle\Model\Maintenance as BaseMaintenance;

    /**
     * Maintenance
     *
     * @ORM\Table(name="maintenance")
     * @ORM\Entity(repositoryClass="AppBundle\Repository\MaintenanceRepository")
     */
    class Maintenance extends BaseMaintenance
    {
        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Get id
         *
         * @return int
         */
        public function getId()
        {
            return $this->id;
        }
    }


5) //-- enter these commands and then insert one line in database with id = 1 and name = name of your site --//

    php bin/console doctrine:generate:entities App
    php bin/console doctrine:schema:update --force


6) //-- update your front controller  --//
   //-- use only render() method to render view, not renderView() --//
   //-- (if you use both analyticbundle and maintenancebundle, install first analyticbundle and then extends only GKratzController) --//

    <?php

    namespace AppBundle\Controller;

    use Gkratz\MaintenanceBundle\Controller\GKratzMaintenanceController;

    class DefaultController extends GKratzMaintenanceController
    {
        // ...
    }


## How to use

1) //-- feel free to change the text or language of the vendor/gkratz/maintenancebundle/Gkratz/MaintenanceBundle/Resources/views/base/maintenance.html.twig and web/bundles/gkratzmaintenance/js/jquery.countdown.js files --//


2) //-- change your admin controller like this exemple --//

    /**
     * @Route("/admin", name="admin")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        /** @var  $em \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();
        /** @var  $maintenance \AppBundle\Entity\Maintenance */
        $maintenance = $em->getRepository("AppBundle:Maintenance")->find(1);
        return $this->render('default/index.html.twig', array('maintenance' => $maintenance));
    }

    /**
     * @Route("/maintenanceSwitch/{state}", name="maintenanceSwitch")
     * @Method({"GET"})
     * @param $state
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function maintenanceSwitch($state){
        /** @var  $em \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();
        /** @var  $maintenance \AppBundle\Entity\Maintenance */
        $maintenance = $em->getRepository("AppBundle:Maintenance")->find(1);
        $maintenance->setMaintenance($state);
        $em->flush();
        return $this->redirectToRoute("admin");
    }

    /**
     * @Route("/maintenanceIp", name="maintenanceIp")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function maintenanceIp(){
        /** @var  $em \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();
        /** @var  $maintenance \AppBundle\Entity\Maintenance */
        $maintenance = $em->getRepository("AppBundle:Maintenance")->find(1);

        if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        elseif(isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip  = $_SERVER['HTTP_CLIENT_IP'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $maintenance->setIp($ip);
        $em->flush();
        return $this->redirectToRoute("admin");
    }

    /**
     * @Route("/maintenanceDate", name="maintenanceDate")
     * @Method({"GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function maintenanceDate(Request $request){
        /** @var  $em \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();
        /** @var  $maintenance \AppBundle\Entity\Maintenance */
        $maintenance = $em->getRepository("AppBundle:Maintenance")->find(1);
        $date = $request->query->get('date');
        $time = $request->query->get('time');
        try{
            $datetime = new \DateTime($date.' '.$time);
            $maintenance->setDate($datetime);
            $em->flush();
            return $this->redirectToRoute("admin");
        }catch(\Exception $e){
            die ($e->getMessage());
        }
    }


3) //-- insert your buttons into the index.html file (feel free to use datepicker, a switch button, font-awesome and other cool stuff) --//

    {% if maintenance.maintenance == 0 %}
        <a href="{{ path('maintenanceSwitch', {'state': 1}) }}">Put maintenance On</a>
    {% else %}
        <a href="{{ path('maintenanceSwitch', {'state': 0}) }}">Put maintenance Off</a>
    {% endif %}
    <p>Exception IP: {{ maintenance.ip }}</p>
    <a href="{{ path('maintenanceIp') }}">Set my ip for exception IP</a>
    <form action="{{ path('maintenanceDate') }}" method="get">
        <input type="date" name="date" placeholder="{{ maintenance.date|date('Y-m-d') }}" required>
        <input type="time" name="time" placeholder="{{ maintenance.date|date('H:i') }}" required>
        <input type="submit">
    </form>

