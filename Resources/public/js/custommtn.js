/*Theme    : Quick
 * Author  : Design_mylife
 * Version : V1.0
 * 
 */


// ***********************************
// Backstretch - Slider on Background
// ***********************************								  
			 
$("body").backstretch([
   "/bundles/gkratzmaintenance/img/showcase-5.jpg",
   "/bundles/gkratzmaintenance/img/showcase-3.jpg",
   "/bundles/gkratzmaintenance/img/showcase-4.jpg"
], {duration: 5000, fade: 1000});
							  


