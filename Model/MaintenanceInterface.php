<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 01/11/2016
 * Time: 23:26
 */

namespace Gkratz\MaintenanceBundle\Model;


/**
 * Interface MaintenanceInterface
 * @package Gkratz\MaintenanceBundle\Model
 */
interface MaintenanceInterface
{
    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Maintenance
     */
    public function setDate($date);

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate();

    /**
     * Set name
     *
     * @param string $name
     * @return Maintenance
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set ip
     *
     * @param string $ip
     * @return Maintenance
     */
    public function setIp($ip);

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp();

    /**
     * Set maintenance
     *
     * @param boolean $ip
     * @return Maintenance
     */
    public function setMaintenance($maintenance);

    /**
     * Get maintenance
     *
     * @return boolean
     */
    public function getMaintenance();

    /**
     * Set phone
     *
     * @param string $phone
     * @return Maintenance
     */
    public function setPhone($phone);

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone();

    /**
     * Set email
     *
     * @param string $email
     * @return Maintenance
     */
    public function setEmail($email);

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail();

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Maintenance
     */
    public function setFacebook($facebook);

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook();

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Maintenance
     */
    public function setTwitter($twitter);

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter();
}